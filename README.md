## Wownero Explorer

A simple web interface for searching the Wownero blockchain.

## Quick-Start

### Docker

The fastest way to get it up and running to test it locally is to use the pre-made Docker container:

```
export DAEMON_URI=http://so.wow.candy.surf:34568
docker run -d \
    --name wownero-explorer \
    --rm \
    -p 80:8000 \
    -e DAEMON_URI=$DAEMON_URI \
    lalanza808/wownero-explorer:latest
```

You could also just use `docker-compose`, though it will download a reverse proxy image:

```
export DAEMON_URI=http://so.wow.candy.surf:34568
docker-compose up -d
```

Navigate to http://localhost/ to browse the explorer.


### Rust

Alternatively, if you'd like to build/run using native Rust tools, follow these steps:

1. Install Rust: https://www.rust-lang.org/tools/install
2. Clone this repo: `git clone https://git.wownero.com/lza_menace/wownero-explorer && cd wownero-explorer`
3. Pin the nightly version of rust to the local directory: `rustup override set nightly`
4. [Pick a Wownero node](https://wownero.com) if you don't have one and apply the `DAEMON_URI` environment variable: `export DAEMON_URI=http://so.wow.candy.surf:34568`
5. Run the application: `cargo run`
6. Navigate to http://localhost:8000/


## Full Installation

I'm using [Traefik](https://docs.traefik.io/getting-started/concepts/) as a reverse proxy in order to handle automatic TLS certification with Let's Encrypt and to avoid manage Nginx configs. If you want to run the full installation you will need a registered domain and control of DNS records.

I don't feel like paying for a cluster for this simple app so I'm using `docker-compose` on a single box that I can recreate easily and often to avoid config drift.

1. Create new file, `docker-compose.prod.yml` as a copy of `docker-compose.yml`
2. Uncomment labels under `wownero-explorer` service
3. Change labels to match external DNS name
4. Set `${ACME_EMAIL}` either as hardcoded string or pass as environment variable
5. Run `docker-compose -f docker-compose.prod.yml up -d`


## Questions?

Ask around in IRC: https://webchat.freenode.net/?room=#wownero
