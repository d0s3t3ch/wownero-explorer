{% extends "base" %}

{% block content %}

<section id="main">
  <div class="container">
    <section id="header">
        <nav id="nav">
          <a href="/"><h1>Go Back</h1>
            <img src="//files.lzahq.tech/wow/WOW-Teleporter-720x1180.png" height=200 />
          </a>
        </nav>
    </section>
    <div class="col-12">
        <section>
          <header class="major">
            <h2>Transaction {{ tx_hash | truncate(length=8) }}</h2>
            <p class="subheader"><strong>Full Hash</strong>: {{ tx_hash }}</p>
            <p class="subheader"><strong>Block Timestamp</strong>: {% if tx_info.0.block_timestamp %}{{ tx_info.0.block_timestamp | date(format="%Y-%m-%d %H:%M") }} UTC{% else %}?{% endif %}</p>
          </header>
          <div class="row">
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>Block Height</h3>
                </header>
                <p><a href="/block/height/{{ tx_info.0.block_height }}">{{ tx_info.0.block_height }}</a></p>
              </section>
            </div>
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>Transaction Fee</h3>
                </header>
                <p>{% if tx_info.0.as_json_full.rct_signatures.txnFee %}{{ tx_info.0.as_json_full.rct_signatures.txnFee / 10000000000 }} WOW{% else %}0 (mined){% endif %}</p>
              </section>
            </div>
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>Double Spend</h3>
                </header>
                <p>{{ tx_info.0.double_spend_seen }}</p>
              </section>
            </div>
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>In Tx Pool</h3>
                </header>
                <p>{{ tx_info.0.in_pool }}</p>
              </section>
            </div>
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>Version</h3>
                </header>
                <p>{{ tx_info.0.as_json_full.version | default(value="?") }}</p>
              </section>
            </div>
            <div class="col-4 col-6-medium col-12-small">
              <section class="box">
                <header>
                  <h3>Inputs / Outputs</h3>
                </header>
                <p>{{ tx_info.0.as_json_full.vin | length | default(value="?") }} / {{ tx_info.0.as_json_full.vout | length | default(value="?") }}</p>
              </section>
            </div>
          </div>
        </section>
        <section>
          <h1 class="center">Prove Sending</h1>
          <form method="get" action="/receipt" class="center">
            <input type="text" name="tx_hash" placeholder="Transaction Hash / ID" value="{{ tx_hash }}" type="hidden" class="hidden">
            <input type="text" name="address" placeholder="Address">
            <input type="text" name="tx_key" placeholder="Transaction Key">
            <input type="submit" value="Prove" class="btn">
          </form>
        </section>
      <div class="tx-table">
        <table>
          <caption><h2>Inputs ({{ tx_info.0.as_json_full.vin | length | default(value="?") }})</h2></caption>
          {% if tx_info.0.as_json_full %}
          <tr>
            <th>Key Images</th>
          </tr>
            {% for i in tx_info.0.as_json_full.vin %}
              <tr>
                <td>{{ i.key.k_image | default(value="none (mined)") }}</td>
              </tr>
            {% endfor %}
          {% endif %}
        </table>
      </div>
      <div class="tx-table">
        <table>
          <caption><h2>Outputs ({{ tx_info.0.as_json_full.vout | length | default(value="?") }})</h2></caption>
          {% if tx_info.0.as_json_full %}
          <tr>
            <th>Stealth Addresses</th>
          </tr>
            {% for i in tx_info.0.as_json_full.vout %}
              <tr>
                <td>{{ i.target.key }}</td>
              </tr>
            {% endfor %}
          {% endif %}
        </table>
      </div>
      <div class="debug">
        <table>
        <caption><h2>Debug Output</h2></caption>
        </table>
        <p>{{ debug | json_encode() }}</p>
      </div>
    </div>
</section>

<style>
  .center {
    margin: 0 auto;
    display: block;
    text-align: center;
  }
  .hidden {
    display: none;
  }
  h1 {
    font-size: 2em;
    padding-bottom: 1em;
  }
  input[type=text] {
    width: 50%;
    margin: 1em;
  }
  input[type=submit] {
    margin: 1em;
  }
</style>

{% endblock content %}
