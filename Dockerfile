FROM ubuntu:20.04 as builder

# Install required packages
RUN apt-get update && apt-get install -y build-essential
RUN apt-get install -y curl git

# Setup app user and workspace
RUN useradd -m -d /home/wownero-explorer wownero-explorer
RUN mkdir -p /srv/wownero-explorer && chown -R wownero-explorer:wownero-explorer /srv
USER wownero-explorer
WORKDIR /srv/wownero-explorer

# Install Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | RUSTUP_HOME=/home/wownero-explorer/.rustup sh -s -- -y

# Setup dummy build and perform release
COPY dummy.rs .
COPY Cargo.toml .
COPY Cargo.lock .
RUN sed -i 's_src/main.rs_dummy.rs_' Cargo.toml
RUN ~/.cargo/bin/rustup override set nightly
RUN ~/.cargo/bin/cargo build --release

# Put real source onto image and perform release
RUN sed -i 's_dummy.rs_src/main.rs_' Cargo.toml
COPY src src
RUN ~/.cargo/bin/cargo build --release

FROM ubuntu:20.04
COPY --from=builder /srv/wownero-explorer/target/release/wownero-explorer /bin/wownero-explorer
COPY static static
COPY templates templates

CMD 'wownero-explorer'
